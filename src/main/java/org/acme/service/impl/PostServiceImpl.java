package org.acme.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import org.acme.dto.request.PostReqDto;
import org.acme.dto.response.PostDto;
import org.acme.model.Post;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.service.PostService;

import java.util.List;

@ApplicationScoped
public class PostServiceImpl implements PostService {
    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;

    @Transactional
    @Override
    public Post save(PostReqDto postReq) throws WebApplicationException {
        var post = Post.builder()
                .title(postReq.getTitle())
                .content(postReq.getContent())
                .build();

        validate(post);

        if (this.postRepository.findByTitle(postReq.getTitle()).isPresent()) {
            throw new WebApplicationException("Post title already exists");
        }

        for (String tagStr : postReq.getTags()) {
            var tag = this.tagRepository.findByLabel(tagStr)
                    .orElseGet(
                            () -> Tag.builder()
                                    .label(tagStr)
                                    .build()
                    );
            post.addTag(tag);
        }
        this.postRepository.persist(post);
        return post;
    }

    @Transactional
    @Override
    public Post update(Long id, PostReqDto postReq) throws WebApplicationException {
        var post = this.postRepository.findById(id);
        if (post == null) throw new WebApplicationException("Post not found");

        post.setTitle(postReq.getTitle());
        post.setContent(postReq.getContent());

        validate(post);

        var checkTitle = this.postRepository.findByTitle(postReq.getTitle());
        if (checkTitle.isPresent() && !checkTitle.get().getId().equals(id)) {
            throw new WebApplicationException("Post title already exists");
        }

        for (String tagStr : postReq.getTags()) {
            var tag = this.tagRepository.findByLabel(tagStr)
                    .orElseGet(
                            () -> Tag.builder()
                                    .label(tagStr)
                                    .build()
                    );
            post.addTag(tag);
        }
        this.postRepository.persist(post);
        return post;
    }

    @Override
    public List<Post> all() {
        return this.postRepository.listAll();
    }

    @Transactional
    @Override
    public void delete(Long id) throws WebApplicationException {
        var post = this.postRepository.findById(id);
        if (post == null) throw new WebApplicationException("Post not found");
        this.postRepository.delete(post);
    }

    private void validate(Post post) throws WebApplicationException {
        if (post.getTitle() == null || post.getTitle().isEmpty())
            throw new WebApplicationException("Post title cannot be null or empty");
        if (post.getContent() == null || post.getContent().isEmpty())
            throw new WebApplicationException("Post content cannot be null or empty");
    }
}
