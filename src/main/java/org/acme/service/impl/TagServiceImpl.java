package org.acme.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import org.acme.dto.request.TagReqDto;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.service.TagService;

import java.util.List;

@ApplicationScoped
public class TagServiceImpl implements TagService {
    @Inject
    TagRepository tagRepository;

    @Inject
    PostRepository postRepository;

    @Override
    public List<Tag> all() {
        return this.tagRepository.listAll();
    }

    @Transactional
    @Override
    public Tag save(TagReqDto tagReq) throws WebApplicationException {
        validate(tagReq);

        if (this.tagRepository.findByLabel(tagReq.getLabel()).isPresent()) {
            throw new WebApplicationException("Tag label already exists");
        }

        var tag = Tag.builder()
                .label(tagReq.getLabel())
                .build();
        this.tagRepository.persist(tag);
        return tag;
    }

    @Transactional
    @Override
    public Tag update(Long id, TagReqDto tagReq) throws WebApplicationException {
        validate(tagReq);

        var tag = this.tagRepository.findById(id);
        if (tag == null) throw new WebApplicationException("Tag not found");

        var checkLabel = this.tagRepository.findByLabel(tagReq.getLabel());
        if (checkLabel.isPresent() && !checkLabel.get().getId().equals(id)) {
            throw new WebApplicationException("Tag label already exists");
        }

        tag.setLabel(tagReq.getLabel());
        this.tagRepository.persist(tag);
        return tag;
    }

    @Transactional
    @Override
    public void delete(Long id) throws WebApplicationException {
        var tag = this.tagRepository.findById(id);
        if (tag == null) {
            throw new WebApplicationException("Tag not found");
        }

        tag.getPosts().forEach(post -> {
            post.getTags().remove(tag);
            this.postRepository.persist(post);
        });

        this.tagRepository.delete(tag);
    }

    private void validate(TagReqDto tag) throws WebApplicationException {
        if (tag.getLabel() == null || tag.getLabel().isEmpty())
            throw new WebApplicationException("Tag label cannot be null or empty");
    }
}
