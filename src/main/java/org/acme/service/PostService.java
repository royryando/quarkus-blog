package org.acme.service;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.WebApplicationException;
import org.acme.dto.request.PostReqDto;
import org.acme.dto.response.PostDto;
import org.acme.model.Post;

import java.util.List;

public interface PostService {
    Post save(PostReqDto postReq) throws WebApplicationException;
    Post update(Long id, PostReqDto postReq) throws WebApplicationException;
    List<Post> all();
    void delete(Long id) throws WebApplicationException;
}
