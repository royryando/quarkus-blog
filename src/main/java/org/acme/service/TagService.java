package org.acme.service;

import jakarta.ws.rs.WebApplicationException;
import org.acme.dto.request.PostReqDto;
import org.acme.dto.request.TagReqDto;
import org.acme.model.Post;
import org.acme.model.Tag;

import java.util.List;

public interface TagService {
    List<Tag> all();
    Tag save(TagReqDto tagReq) throws WebApplicationException;
    Tag update(Long id, TagReqDto tagReq) throws WebApplicationException;
    void delete(Long id) throws WebApplicationException;
}
