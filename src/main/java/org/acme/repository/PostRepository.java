package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.acme.model.Post;

import java.util.Optional;

@ApplicationScoped
public class PostRepository implements PanacheRepository<Post> {
    public Optional<Post> findByTitle(String title) {
        return find("title", title)
                .singleResultOptional();
    }
}
