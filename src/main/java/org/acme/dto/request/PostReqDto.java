package org.acme.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class PostReqDto {
    private String title;
    private String content;
    private List<String> tags;
}
