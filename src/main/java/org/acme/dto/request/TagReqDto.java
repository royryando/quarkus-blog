package org.acme.dto.request;

import lombok.Data;

@Data
public class TagReqDto {
    private String label;
}
