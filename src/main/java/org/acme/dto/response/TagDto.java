package org.acme.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class TagDto {
    private Long id;
    private String label;
    private Set<PostBaseDto> posts = new HashSet<>();
}
