package org.acme.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostBaseDto {
    private Long id;
    private String title;
    private String content;
}
