package org.acme.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagBaseDto {
    private Long id;
    private String label;
}
