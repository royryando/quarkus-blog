package org.acme.utils;

import org.acme.dto.response.TagBaseDto;
import org.acme.model.Tag;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(
        componentModel = "cdi",
        uses = {PostBaseMapper.class}
)
public interface TagBaseMapper {
    TagBaseDto to(Tag tag);
    Set<TagBaseDto> to(List<Tag> tags);
}
