package org.acme.utils;

import org.acme.dto.response.TagDto;
import org.acme.model.Tag;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(
        componentModel = "cdi",
        uses = {TagBaseMapper.class}
)
public interface TagMapper {
    TagDto to(Tag tag);
    Set<TagDto> to(List<Tag> tags);
}

