package org.acme.utils;

import org.acme.dto.response.PostDto;
import org.acme.model.Post;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(
        componentModel = "cdi",
        uses = {TagMapper.class, PostBaseMapper.class, TagBaseMapper.class}
)
public interface PostMapper {
    PostDto to(Post post);
    Set<PostDto> to(List<Post> posts);
}
