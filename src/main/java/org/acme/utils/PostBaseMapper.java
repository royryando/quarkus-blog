package org.acme.utils;

import org.acme.dto.response.PostBaseDto;
import org.acme.model.Post;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "cdi")
public interface PostBaseMapper {
    PostBaseDto to(Post post);
    Set<PostBaseDto> to(List<Post> posts);
}

