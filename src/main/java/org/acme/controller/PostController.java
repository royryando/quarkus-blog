package org.acme.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.acme.dto.request.PostReqDto;
import org.acme.dto.response.PostDto;
import org.acme.service.PostService;
import org.acme.utils.PostMapper;

import java.util.Set;

@Path("/api/posts")
public class PostController {
    @Inject
    PostService postService;

    @Inject
    PostMapper postMapper;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<PostDto> posts() {
        var result = this.postService.all();
        return this.postMapper.to(result);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PostDto createPost(PostReqDto postReqDto) {
        var result = this.postService.save(postReqDto);
        return this.postMapper.to(result);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PostDto createPost(@PathParam("id") Long id, PostReqDto postReqDto) {
        var result = this.postService.update(id, postReqDto);
        return this.postMapper.to(result);
    }

    @DELETE
    @Path("/{id}")
    public String deletePost(@PathParam("id") Long id) {
        this.postService.delete(id);
        return "Post " + id + " successfully deleted";
    }

}
