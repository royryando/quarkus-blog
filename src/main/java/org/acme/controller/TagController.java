package org.acme.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.acme.dto.request.TagReqDto;
import org.acme.dto.response.TagDto;
import org.acme.service.TagService;
import org.acme.utils.TagMapper;

import java.util.Set;

@Path("/api/tags")
public class TagController {
    @Inject
    TagService tagService;

    @Inject
    TagMapper tagMapper;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<TagDto> tags() {
        var result = this.tagService.all();
        return this.tagMapper.to(result);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TagDto createTag(TagReqDto postReqDto) {
        var result = this.tagService.save(postReqDto);
        return this.tagMapper.to(result);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TagDto createTag(@PathParam("id") Long id, TagReqDto postReqDto) {
        var result = this.tagService.update(id, postReqDto);
        return this.tagMapper.to(result);
    }

    @DELETE
    @Path("/{id}")
    public String deleteTag(@PathParam("id") Long id) {
        this.tagService.delete(id);
        return "Tag " + id + " successfully deleted";
    }
}
